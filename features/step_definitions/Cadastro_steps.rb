Dado("que eu estou no formulário de cadastro") do
    @login_page.acessa
    @principal_page.fornecedor
end

Dado("possuo os seguintes dados:") do |usuario|
   nome1 = usuario.raw[0]
   @nome_usuario = nome1[1]
   sobrenome1 = usuario.raw[1]
   @sobrenome = sobrenome1[1]
   email1 = usuario.raw[2]
   @email_usuario = email1[1]
   senha = usuario.raw[3]
   @senha_usuario = senha[1]
   pais1 = usuario.raw[4]
   @pais = pais1[1]
end
  
Quando("faço o meu cadastro") do
    @cadastro_page.registrar(@nome_usuario, @sobrenome, @email_usuario, @senha_usuario, @pais)
end
  
Então("vejo o email {string}") do |email|
    expect(@cadastro_page.verifica_cadastro).to have_content email
end

