Dado('que eu acesso o sistema') do
  @login_page.acessa
end

Quando('faço login com {string} e {string}') do |email, senha|
  @login_page.logar(email, senha)
end

Então('vejo o nome do usuário {string}') do |nome_usuario|
  expect(@principal_page.painel).to have_content nome_usuario
end


