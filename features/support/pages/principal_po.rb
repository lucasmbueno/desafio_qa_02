
class PrincipalPage
  include Capybara::DSL

  def painel
    find('.social-sidebar-content')
  end

  def fornecedor
    find('.social-sidebar-content a[href*=ACCOUNTS]').click
    find('.social-sidebar-content a[href*=suppliers]').click
    find('#content > div > form > button').click
  end
end

