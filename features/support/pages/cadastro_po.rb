class CadastroPage

    include Capybara::DSL

    def verifica_cadastro
      find('#content > div > div.panel-body')
    end
  
    def registrar(nome, sobrenome, email, senha, pais)
      find('input[name=fname]').set nome
      find('input[name=lname]').set sobrenome
      find('input[name=email]').set email
      find('input[name=password]').set senha
      find('.select2-choice').click
      find('#select2-drop > div > input').set pais
      find('#select2-drop > ul > li.select2-results-dept-0.select2-result.select2-result-selectable.select2-highlighted > div').click
      find('#content > form > div > div.panel-footer > button').click
      
    end

    def cadastrar
        find('a[href$=register]').click
    end
    
    def alerta_cadastro
      find('#register_form')
    end
  
end
  