Before do
  @login_page = LoginPage.new
  @principal_page = PrincipalPage.new
  @cadastro_page = CadastroPage.new
  page.current_window.resize_to(1440, 900)
end

Before('@login') do
  @usuario = { email: 'admin@phptravels.com', senha: 'demoadmin' }

  @login_page.acessa
  @login_page.logar(@usuario[:email], @usuario[:senha])
end

After('@logout') do
  find('body > div.wrapper > aside > div > div.col-md-12 > div > div:nth-child(2) > div > a').click
end

