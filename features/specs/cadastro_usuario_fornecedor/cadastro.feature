#language: pt

Funcionalidade: Cadastro
    Para que eu possa usuar o site e gcadastrar novos fornecedores
    Preciso preencher um formulário de cadastro
    
    Contexto: Formulário
        Dado que eu estou no formulário de cadastro

    @login @cadastro @logout
    Cenário: Cadastro simples

        E possuo os seguintes dados:
            | nome  | Lucas            |
            | last  | Bueno            |
            | email | lucas@lucas.net  |
            | senha | 123456           |
            | pais  | Argentina        |
        Quando faço o meu cadastro 
        Então vejo o email "lucas@lucas.net"

