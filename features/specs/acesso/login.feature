#language: pt

Funcionalidade: Login
    Para que eu possa cadastrar novos usuarios
    Sendo um usuário
    Posso acessar o sistema com meu email e senha previamente cadastrados.

    Contexto: Home
        Dado que eu acesso o sistema

    @logout @login
    Cenario: Usuário autenticado
        
        Quando faço login com "admin@phptravels.com" e "demoadmin"
        Então vejo o nome do usuário "Super Admin"

    
    